% testing for change

%% setup joystick
id = 1; % Note: may need to be changed if multiple joysticks present
joy = vrjoystick(id);
caps(joy) % display joystick information

%% Set up robot
mdl_puma560;                    % Load Puma560
robot = p560;                   % Create copy called 'robot'
robot.tool = transl(0.1,0,0)  % Define tool frame on end-effector
qMatrix = [pi,pi,pi,pi,pi,pi];
% T1 = [eye(3) [1.5 1 0]'; zeros(1,3) 1];
% T2 = [eye(3) [1.5 -1 0]'; zeros(1,3) 1] 
T1 = zeros(1,6);
deltaT = 0.05;

%steps = 50;

%% Start "real-time" simulation
q = qn;                 % Set initial robot configuration 'q'

HF = figure(1);         % Initialise figure to display robot
robot.plot(q);          % Plot robot in initial configuration
robot.delay = 0.001;    % Set smaller delay when animating
set(HF,'Position',[0.1 0.1 0.8 0.8]);

duration = 30;  % Set duration of the simulation (seconds)
dt = 0.15;      % Set time step for simulation (seconds)

n = 0;  % Initialise step count to zero 
tic;    % recording simulation start time
while( toc < duration)
    
    n=n+1; % increment step count

    % read joystick
    [axes, buttons, povs] = read(joy);
       
    % -------------------------------------------------------------
    % YOUR CODE GOES HERE
    % 1 - turn joystick input into an end-effector velocity command
    % 2 - use J inverse to calculate joint velocity
    % 3 - apply joint velocity to step robot joint angles 
    % -------------------------------------------------------------
    
    K1 = 0.3;
%     K2 = 0.8;
    vx = K1*axes(2);
    vy = K1*axes(1);
    vz = K1*axes(6);
    wx =0;
    wy =0;
    wz =0;
  
%     wx = K2*(buttons(4)-buttons(2))
%     wy = K2*(buttons(4)-buttons(2))
%     wz = K2*(buttons(4)-buttons(3))
    
%     a = p560.fkine(qMatrix)
%     a(1:3,4)
%     qMatrix = p560.ikine(a)
   
    
%     x = [vx vy vz wx wy wz]'
%     if (abs(vx)<0.02 && abs(vy)<0.02 && abs(vz)<0.02 )
%         display('a');
 
%     else
    xdot = [vx vy vz wx wy wz]'
    
%     qMatrix = zeros(steps,6);
    J = p560.jacob0(qMatrix);            % Get the Jacobian at the current state
    %J = J(1:2,:);                           % Take only first 2 rows
    qdot = inv(J)*xdot;                             % Solve velocitities via RMRC
    qMatrix =  qMatrix + deltaT*qdot';
    
%     p560.plot(qMatrix,'trail','r-');
%    pause(1)
%     end
% x1 = [1.5 1]';
% x2 = [1.5 -1]';
% deltaT = 0.05;                                        % Discrete time step
% 
% x = zeros(2,steps);
% s = lspb(0,1,steps);                                 % Create interpolation scalar
% for i = 1:steps
%     x(:,i) = x1*(1-s(i)) + s(i)*x2;                  % Create trajectory in x-y plane
% end
    
%     qMatrix = nan(steps,6);
%     
%     qMatrix(1,:) = p560.ikine(T1);                 % Solve for joint angles
%     
%     for i = 1:steps-1
%         xdot = (x(:,i+1) - x(:,i))/deltaT;                             % Calculate velocity at discrete time step
%         J = p560.jacob0(qMatrix(i,:));            % Get the Jacobian at the current state
%         J = J(1:2,:);                           % Take only first 2 rows
%         qdot = inv(J)*xdot;                             % Solve velocitities via RMRC
%         qMatrix(i+1,:) =  qMatrix(i,:) + deltaT*qdot';                   % Update next joint state
%     end
% 
% p2.plot(qMatrix,'trail','r-');

    % Update plot
    robot.animate(qMatrix);  
    
    % wait until loop time elapsed
    if (toc > dt*n)
        warning('Loop %i took too much time - consider increating dt',n);
    end
    while (toc < dt*n); % wait until loop time (dt) has elapsed 
    end
end
      
