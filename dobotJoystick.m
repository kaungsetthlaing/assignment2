%% setup joystick
id = 1; % Note: may need to be changed if multiple joysticks present
joy = vrjoystick(id);
caps(joy) % display joystick information

%% Set up robot
% mdl_puma560;                    % Load Puma560
% robot = p560;                   % Create copy called 'robot'
% robot.tool = transl(0.1,0,0)  % Define tool frame on end-effector

% T1 = [eye(3) [1.5 1 0]'; zeros(1,3) 1];
% T2 = [eye(3) [1.5 -1 0]'; zeros(1,3) 1] 


%steps = 50;

L1 = Link('d',0.05,'a',0,'alpha',-pi/2,'offset',0,'qlim',[deg2rad(-135),deg2rad(135)]) %joint 1 

L2 = Link('d',0,'a',0.135,'alpha',0,'offset',-pi/2,'qlim',[deg2rad(5),deg2rad(80)]) %joint 2

L3 = Link('d',0,'a',0.160,'alpha',0,'offset',0,'qlim',[deg2rad(15),deg2rad(170)]) %joint 3

L4 = Link('d',0,'a',0.05,'alpha',-pi/2, 'offset',0, 'qlim',[-pi/2,pi/2]) %joint 4

L5 = Link('d',0.05,'a',0,'alpha',0,'offset',0 ,'qlim',[deg2rad(-85),deg2rad(85)]) %joint 5

myRobot1 = SerialLink([L1 L2 L3 L4 L5], 'name', 'Dobot');
for linkIndex = 0:myRobot1.n      %% arm 1
    [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['Link',num2str(linkIndex),'.ply'],'tri'); %%use plyread to get the 3d models
    myRobot1.faces{linkIndex+1} = faceData;   %assign corresponding faces and vertices 
    myRobot1.points{linkIndex+1} = vertexData;
end

q = zeros(1,5)

workspace = [-0.5 0.5 -0.5 0.5 -0.5 0.5];

% qMatrix = DobotIkine(myRobot1,0.15,0.1,0.03);
qMatrix = DobotIkine(myRobot1,0.1,0.1,0.1);

HF = figure(1); 
myRobot1.plot3d(qMatrix,'workspace',workspace);
robot.delay = 0.001;
% set(HF,'Position',[0.1 0.1 0.8 0.8]);

T1 = zeros(1,6);
deltaT = 0.05;

%% Start "real-time" simulation
% q = qn;                 % Set initial robot configuration 'q'

% HF = figure(1);         % Initialise figure to display robot
% robot.plot(q);          % Plot robot in initial configuration
% robot.delay = 0.001;    % Set smaller delay when animating
% set(HF,'Position',[0.1 0.1 0.8 0.8]);

duration = 30;  % Set duration of the simulation (seconds)
dt = 0.15;      % Set time step for simulation (seconds)

n = 0;  % Initialise step count to zero 
tic;    % recording simulation start time
while( toc < duration)
    
    n=n+1; % increment step count

    % read joystick
    [axes, buttons, povs] = read(joy);
       
    % -------------------------------------------------------------
    % YOUR CODE GOES HERE
    % 1 - turn joystick input into an end-effector velocity command
    % 2 - use J inverse to calculate joint velocity
    % 3 - apply joint velocity to step robot joint angles 
    % -------------------------------------------------------------
    
    K1 = 0.3;
    K2 = 0.8;
    vx = K1*axes(2);
    vy = K1*axes(1);
    vz = K1*axes(6);
    wx =0;
    wy =0;
    wz =0;
%     wx = K2*(buttons(4)-buttons(2))
%     wy = K2*(buttons(4)-buttons(2))
%     wz = K2*(buttons(4)-buttons(1))
    
%     a = p560.fkine(qMatrix)
%     a(1:3,4)
%     qMatrix = p560.ikine(a)
   
    
%     x = [vx vy vz wx wy wz]'
%     if (abs(vx)<0.02 && abs(vy)<0.02 && abs(vz)<0.02 )
%         display('a');
 
%     else
    xdot = [vx vy vz wy wz]'
    
    qMatrix = DobotIkine(myRobot1,0.1,0.1,0.1);
%     qMatrix = zeros(steps,6);
    J = myRobot1.jacob0(qMatrix)            % Get the Jacobian at the current state
    J = J(1:5,:)                           % Take only first 5 rows
    qdot = inv(J)*xdot                             % Solve velocitities via RMRC
    qMatrix =  qMatrix + deltaT*qdot';
    
%     p560.plot(qMatrix,'trail','r-');
%    pause(1)
%     end
% x1 = [1.5 1]';
% x2 = [1.5 -1]';
% deltaT = 0.05;                                        % Discrete time step
% 
% x = zeros(2,steps);
% s = lspb(0,1,steps);                                 % Create interpolation scalar
% for i = 1:steps
%     x(:,i) = x1*(1-s(i)) + s(i)*x2;                  % Create trajectory in x-y plane
% end
    
%     qMatrix = nan(steps,6);
%     
%     qMatrix(1,:) = p560.ikine(T1);                 % Solve for joint angles
%     
%     for i = 1:steps-1
%         xdot = (x(:,i+1) - x(:,i))/deltaT;                             % Calculate velocity at discrete time step
%         J = p560.jacob0(qMatrix(i,:));            % Get the Jacobian at the current state
%         J = J(1:2,:);                           % Take only first 2 rows
%         qdot = inv(J)*xdot;                             % Solve velocitities via RMRC
%         qMatrix(i+1,:) =  qMatrix(i,:) + deltaT*qdot';                   % Update next joint state
%     end
% 
% p2.plot(qMatrix,'trail','r-');

    % Update plot
    robot.animate(qMatrix);  
    
    % wait until loop time elapsed
    if (toc > dt*n)
        warning('Loop %i took too much time - consider increating dt',n);
    end
    while (toc < dt*n); % wait until loop time (dt) has elapsed 
    end
end
      
