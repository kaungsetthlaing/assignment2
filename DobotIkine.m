function q = DobotIkine(dobot , x, y, z )
robot = dobot
a2= robot.links(2).a;
a3= robot.links(3).a;

q1=atan(y/x)
 if x<0 
     q1=q1+pi
 end
 
x=x-cos(q1)*robot.links(4).a;
y=y-sin(q1)*robot.links(4).a;
z=z-robot.links(1).d+robot.links(5).d

l=sqrt(x^2+y^2);
D=sqrt(z^2+l^2);
t1=atan(z/l);
t2=acos((a2^2+D^2-a3^2)/2/a2/D);
apha=t1+t2;
beta=acos((a2^2-D^2+a3^2)/2/a2/a3);
q2=pi/2-apha;

q3=3*pi/2-beta-apha-q2;
q4 = pi/2- q3 - q2
q5 =0

q=[q1 q2 q3 q4 q5];

end
